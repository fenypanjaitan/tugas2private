package com.example.demo.pr01ClassObject;

/*
* Object adalah sebuah variabel yang merupakan instance atau
* perwujudan dari Class yang memiliki states dan behavior.
*
* Class adalah blue print dari objek.
*
* */

public class Dog {
    public Dog(String name){
        System.out.println("nama anjingnya "+ name);
    }
    public static void main(String[] args) {
        Dog dog = new Dog("combat");

    }
}
