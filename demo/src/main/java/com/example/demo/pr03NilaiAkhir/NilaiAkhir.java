package com.example.demo.pr03NilaiAkhir;

public class NilaiAkhir {
    public static void main(String[] args) {
        String[] nama={"Ardi","Pian", "Robi"};
        int[][] arrayNilai = {{60, 70, 90},
                {80, 70, 90},
                {70, 60, 80}};
        double nilai;
        System.out.println("+-------+-------+-------+-------+-------------+");
        System.out.println("|  Nama |  UTS  |  UAS  | TUGAS | NILAI AKHIR |");
        System.out.println("+-------+-------+-------+-------+-------------+");

        for (int row = 0; row < 3; row++){
            System.out.print("| " +  nama[row] + " \t|  ");
            for (int column = 0; column < 3; column++){
                System.out.print(arrayNilai[row][column] + "\t| ");
            }
            nilai = (0.35 * arrayNilai[row][0]) + (0.45 * arrayNilai[row][1]) + (0.2 * arrayNilai[row][2]);
            System.out.println(nilai + "\t      |");
        }
        System.out.println("+-------+-------+-------+-------+-------------+");

    }
}
