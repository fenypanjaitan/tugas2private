package com.example.demo.pr04BangunRuang;

import java.text.DecimalFormat;

public class BangunRuang {
    int panjang = 30;
    int lebar = 5;
    int tinggi = 10;
    int sisi = 9;
    double pi = 3.14;
    double r = 10;
    int volumeBalok = panjang*lebar*tinggi;
    int volumeKubus = sisi*sisi*sisi;
    double volumeBola = pi*r*r*r*4/3;
    DecimalFormat df = new DecimalFormat("#.##");


    public void balok(){
        System.out.println("volume balok: "+volumeBalok);
    }

    public void kubus(){
        System.out.println("volume kubus: "+volumeKubus);
    }

    public void bola(){
        System.out.println("volume bola: "+df.format(volumeBola));
    }

    public void summary(){
        double sum = volumeBalok+volumeBola+volumeKubus;
        System.out.println("jumlah total: "+df.format(sum));
    }

    public void average(){
        double aver = (volumeBalok+volumeBola+volumeKubus)/3;
        System.out.println("Rata-rata: "+df.format(aver));
    }


}
